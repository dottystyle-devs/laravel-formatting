<?php

namespace Dottystyle\LaravelFormatter;

use Dottystyle\LaravelFormatter\Formatters\HasOptions;
use UnexpectedValueException;

class FormatterManager
{
    protected $app;

    protected $formatters;

    protected $instances;

    public function __construct($app, array $formatters)
    {
        $this->app = $app;
        $this->formatters = $formatters;
        $this->instances = [];
    }

    protected function resolve($name)
    {
        if (isset($this->instances[$name])) {
            return $this->instances[$name];
        }

        if (! $this->has($name)) {
            throw new UnexpectedValueException("$name is not a valid formatter");
        }

        $config = $this->formatters[$name];

        $instance = $this->app->make($config['class']);
        unset($config['class']);

        if ($instance instanceof HasOptions) {
            $instance->setOptions($config);
        }

        return $this->instances[$name] = $instance;
    }

    protected function has($name) 
    {
        return isset($this->formatters[$name]);
    }

    public function __get($name)
    {
        return $this->resolve($name);
    }

    public function __call($method, $arguments)
    {
        if ($this->has($method)) {
            return $this->resolve($method)->format(...$arguments);
        }

        return null;
    }
}