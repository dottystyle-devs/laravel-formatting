<?php

namespace Dottystyle\LaravelFormatter\Formatters;

use BadMethodCallException;
use Carbon\Carbon;
use ReflectionMethod;
use UnexpectedValueException;

class Date implements Formatter, HasOptions
{
    use HasOptionsTrait;

    public function format($value, $format = null)
    {
        if ($format && ! $this->isValidFormat($format)) {
            throw new UnexpectedValueException("$format is not registered format");
        }

        $format = $format ?? $this->getOption('default');

        return $this->toString($value, $this->getOption("formats.$format"));
    }

    protected function getFormats()
    {
        return $this->getOption('formats');
    }

    protected function isValidFormat($format)
    {
        return array_key_exists($format, $this->getFormats());
    }

    protected function toString($value, $format)
    {
        $carbon = $value instanceof Carbon ? $value : Carbon::parse($value);

        return $carbon->format($format);
    }

    public function __call($method, $arguments) 
    {
        if (
            $this->isValidFormat($method)
            && (new ReflectionMethod($this, $method))->isPublic()
        ) {
            return $this->format(...$arguments);
        }

        throw new BadMethodCallException("Call to undefined method ".static::class."::$method()");
    }
}