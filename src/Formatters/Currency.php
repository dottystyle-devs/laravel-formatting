<?php

namespace Dottystyle\LaravelFormatter\Formatters;

class Currency implements Formatter, HasOptions
{
    use HasOptionsTrait;

    public function format($value, $currency = null)
    {
        return $this->getSymbol(
            $currency ?: $this->getDefault()
        ).number_format($value, 2);
    }

    protected function getDefault()
    {
        return $this->getOption('default');
    }

    protected function getSymbol($code)
    {
        foreach ($this->getOption('currencies') as $currency) {
            if (strcasecmp($currency['code'], $code) === 0) {
                return $currency['symbol'];
            }
        }

        return '';
    }
}