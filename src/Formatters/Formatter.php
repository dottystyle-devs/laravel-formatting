<?php

namespace Dottystyle\LaravelFormatter\Formatters;

interface Formatter
{
    public function format($value, $options = null);
}
