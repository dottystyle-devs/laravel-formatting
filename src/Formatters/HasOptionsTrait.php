<?php

namespace Dottystyle\LaravelFormatter\Formatters;

trait HasOptionsTrait
{
    protected $options = [];

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getOption($name, $default = null)
    {
        return data_get($this->options, $name, $default);
    }
}