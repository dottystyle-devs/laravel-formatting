<?php

namespace Dottystyle\LaravelFormatter\Formatters;

interface HasOptions
{
    public function setOptions(array $options);
}