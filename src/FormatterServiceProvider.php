<?php

namespace Dottystyle\LaravelFormatter;

use Illuminate\Support\ServiceProvider;

class FormatterServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            $this->packagePath('config.php') => config_path('formatter.php'),
        ]);
    }

    public function register()
    {
        $this->app->singleton('formatter', function ($app) {
            return new FormatterManager(
                $app, $app['config']->get('formatter.formatters')
            );
        });

        $this->mergeConfigFrom(
            $this->packagePath('config.php'), 'formatter'
        );
    }

    public function provides()
    {
        return ['formatter'];
    }

    protected function packagePath($path = '')
    {
        $dir = dirname(__DIR__);

        if ($path) {
            return "$dir/".ltrim($path);
        }

        return $dir;
    }
}