<?php

return [
    'formatters' => [
        'currency' => [
            'class' => \Dottystyle\LaravelFormatter\Formatters\Currency::class,
            'default' => 'USD',
            'currencies' => [
                [
                    'code' => 'USD',
                    'symbol' => '$'
                ]
            ]
        ],
        'date' => [
            'class' => \Dottystyle\LaravelFormatter\Formatters\Date::class,
            'formats' => [
                'full' => 'Y-m-d H:i:s',
                'date' => 'Y-m-d',
                'short' => 'Y-m-d',
                'time' => 'H:i'
            ],
            'default' => 'date'
        ]
    ]
];